# Windows Server

[![Launch Stack](https://s3.amazonaws.com/cloudformation-examples/cloudformation-launch-stack.png)](https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks/new?stackName=windows-ansible&templateURL=https://s3.amazonaws.com/andreswebs.cfn.public/windows-ec2-ansible.yml)

An AWS CloudFormation template which deploys an AWS EC2 instance running Windows Server 2019 configured to be managed by Ansible.

## Author

**Andre Silva** - [andreswebs](https://gitlab.com/andreswebs)
